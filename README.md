# AMD based KVM Hackintosh on OpenSUSE Tumbleweed

**My Setup:**  
CPU: AMD Ryzen 3 1400 (4C)  
RAM: 16GB DDR4 @ 2133 Mhz  
GPU 1: NVIDIA GeForce GT 710  
GPU 2: AMD R9 280 3G  

## KVM Server Setup

### 1. Install OpenSUSE  

Visit [OpenSUSE](https://www.opensuse.org/) to download the latest copy of OpenSUSE Tumbleweed, Tumbleweed is the rolling release distro while Leap 15.1 is the stable release.  

After the ISO has been downloaded copy iso to your thumb drive or DVD ;). If you are using a thumb drive you can utilize this method to copy the file or utilize a tool such at [Etcher](https://www.balena.io/etcher/).

	sudo fdisk -l 
	dmesg | tail # if you have just inserted your thumbdrive
	
	sudo dd_rescue -d -D --force /path/to/opensuse-tumbleweed.iso /dev/sdx

After you have your shiny new OpenSUSE thumb drive, boot your computer from the thumbdrive and proceed to install Tumbleweed.

### 2. Configure your server  

After you boot into the desktop environment, we can install the required tools to be able to get OS X running. I will typicaly add the *Packman Repository* utilizing the *Software Repositories* utlizing the YAST2 tool. After you add the *Packman Repo* scroll down in the YAST tool until you get to *Virtualization,* click *Install Hypervisor and Tools* and select "KVM server" and "KVM tools."  

Click through the following screens to install KVM and QEMU.  

After YAST installs KVM and QEMU open a terminal and install these additional tools:  

	sudo zypper in git vim kernel-source kernel-devel

This will install git so you can clone a repository or two; VIM which is my favorite text editor; kernel-source and kernel-devel for VFIO.


## References
